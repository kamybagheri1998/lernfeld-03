DROP TABLE IF EXISTS departments;
DROP TABLE IF EXISTS device_ip;
DROP TABLE IF EXISTS general_information;

CREATE TABLE "departments" (
	"id_departments"	INTEGER NOT NULL UNIQUE,
	"name_departments"	TEXT NOT NULL,
	"departments_ip_range"	TEXT NOT NULL,
	PRIMARY KEY("id_departments" AUTOINCREMENT)
);

CREATE TABLE "device_ip" (
	"id_devices_ip"	INTEGER NOT NULL UNIQUE,
	"name_device"	TEXT NOT NULL,
	"ip_device"	TEXT NOT NULL,
	departments_id_departments INTEGER NOT NULL,
	PRIMARY KEY("id_devices_ip" AUTOINCREMENT),
	FOREIGN KEY (departments_id_departments) references departments (id_departments)
);

CREATE TABLE "general_information"(
  id_general_information  INTEGER NOT NULL UNIQUE,
  general_information_name TEXT NOT NULL,
  general_information_ip TEXT,
  general_information_pw TEXT

);

