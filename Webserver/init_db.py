import sqlite3

connection = sqlite3.connect('database.db')

with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO departments (id_departments, name_departments,departments_ip_range) VALUES (?, ?,?)",
            (1, 'Verwaltung', '10.1.20.8 - 10.1.20.10')
            )

cur.execute("INSERT INTO departments (id_departments, name_departments,departments_ip_range) VALUES (?, ?,?)",
            (2, 'Finanz', '10.1.20.11 - 10.1.20.12')
            )

cur.execute("INSERT INTO departments (id_departments, name_departments,departments_ip_range) VALUES (?, ?,?)",
            (3, 'Lager', '10.1.30.100 - 10.1.30.101')
            )

cur.execute("INSERT INTO departments (id_departments, name_departments,departments_ip_range) VALUES (?,?,?)",
            (4, 'Einkauf', '10.1.40.101 - 10.1.40.103')
            )

cur.execute("INSERT INTO departments (id_departments, name_departments,departments_ip_range) VALUES (?,?,?)",
            (5, 'Verkauf', '10.1.50.100 - 10.1.50.102')
            )

cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (1, 'Gast-Laptop01', '10.1.80.100', 1)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (2, 'PC-GF01', '10.1.10.8', 1)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (3, 'PC-VW01', '10.1.11.8', 1)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (4, 'Management-Laptop', '10.1.99.253', 1)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (5, 'PC-FI01', '10.1.20.11', 2)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (6, 'PC-FI02', '10.1.20.10', 2)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (7, 'PC-LA01', '10.1.30.10', 3)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (8, 'PC-LA02', '10.1.30.101', 3)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (9, 'PC-EK01', '10.1.40.101', 4)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (10, 'PC-EK02', '10.1.40.102', 4)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (11, 'PC-EK03', '10.1.40.103', 4)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (12, 'PC-EK03', '10.1.40.103', 4)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (13, 'EK-Tablet PC01', '0.0.0.0', 4)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (14, 'PC-VK01', '10.1.50.100', 5)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (15, 'PC-VK02', '10.1.50.101', 5)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (16, 'KD-Smartphone01', '10.1.70.10', 5)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (17, 'MA-Laptop01', '10.1.60.100', 5)
            )
cur.execute("INSERT INTO device_ip (id_devices_ip, name_device,ip_device,departments_id_departments) VALUES (?,?,?,?)",
            (18, 'MA-Tablet01', '10.1.60.101', 5)
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_pw) VALUES (?,?,?)",
            (1, 'Konsolen-Passwort', 'ciscoconpa55')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_pw) VALUES (?,?,?)",
            (2, 'Enablen-Passwort', 'ciscoconpa55')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_ip) VALUES (?,?,?)",
            (3, 'DNS-ServerISP', '8.8.8.8')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_ip) VALUES (?,?,?)",
            (4, 'DNS-ServerISP', '89.12.34.5')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_ip) VALUES (?,?,?)",
            (5, 'E-MAIL', '66.44.33.22')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_pw) VALUES (?,?,?)",
            (6, 'gf', 'ibdC2701')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_pw) VALUES (?,?,?)",
            (7, 'vw01', 'ibVW01')
            )
cur.execute("INSERT INTO general_information (id_general_information, general_information_name,"
            "general_information_pw) VALUES (?,?,?)",
            (8, 'vw02', 'ibVW02')
            )
connection.commit()
connection.close()
