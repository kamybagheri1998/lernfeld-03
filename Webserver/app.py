import csv
import io

from flask import Flask, render_template, Response
import sqlite3


def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/about_us')
def about_us():
    return render_template('about_us.html')


@app.route('/networkPlan')
def network_plan():
    return render_template('networkPlan.html')


@app.route('/ipam')
def ipam():
    return render_template('ipam.html')


@app.route('/port_table')
def port_table():
    return render_template('port_table.html')


@app.route('/action1')
def get_dep():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM departments').fetchall()
    conn.close()
    return render_template('department.html', departments=posts)


@app.route('/action2')
def get_gi():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM general_information').fetchall()
    conn.close()
    return render_template('general.html', general_information=posts)


@app.route('/actionVerwaltung')
def get_verwaltung_device():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM device_ip WHERE departments_id_departments = 1').fetchall()
    conn.close()
    return render_template('device.html', device_ip=posts)


@app.route('/actionFinanz')
def get_finanz_device():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM device_ip WHERE departments_id_departments = 2').fetchall()
    conn.close()
    return render_template('device.html', device_ip=posts)


@app.route('/actionLager')
def get_lager_device():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM device_ip WHERE departments_id_departments = 3').fetchall()
    conn.close()
    return render_template('device.html', device_ip=posts)


@app.route('/actionEinkauf')
def get_einkauf_device():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM device_ip WHERE departments_id_departments = 4').fetchall()
    conn.close()
    return render_template('device.html', device_ip=posts)


@app.route('/actionVerkauf')
def get_verkauf_device():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM device_ip WHERE departments_id_departments = 5').fetchall()
    conn.close()
    return render_template('device.html', device_ip=posts)


@app.route("/donwloadTxtVerwaltung")
def download_report_verwaltung():
    conn = None
    try:
        conn = get_db_connection()
        result = conn.execute(
            "SELECT name_device, ip_device FROM device_ip WHERE departments_id_departments = 1").fetchall()
        output = io.StringIO()
        writer = csv.writer(output)
        line = ['Gerät Name, IP Adresse']
        writer.writerow(line)
        for row in result:
            line = [str(row['name_device']) + ',' + row['ip_device']]
            writer.writerow(line)
        output.seek(0)
        return Response(output, mimetype="text/csv",
                        headers={"Content-Disposition": "attachment;filename=network_report_control.txt"})
    except Exception as e:
        print(e)
    finally:
        conn.close()


@app.route("/donwloadTxtFinanz")
def download_report_finanz():
    conn = None
    try:
        conn = get_db_connection()
        result = conn.execute(
            "SELECT name_device, ip_device FROM device_ip WHERE departments_id_departments = 2").fetchall()
        output = io.StringIO()
        writer = csv.writer(output)
        line = ['Gerät Name, IP Adresse']
        writer.writerow(line)
        for row in result:
            line = [str(row['name_device']) + ',' + row['ip_device']]
            writer.writerow(line)
        output.seek(0)
        return Response(output, mimetype="text/csv",
                        headers={"Content-Disposition": "attachment;filename=network_report_finance.txt"})
    except Exception as e:
        print(e)
    finally:
        conn.close()


@app.route("/donwloadTxtLager")
def download_report_lager():
    conn = None
    try:
        conn = get_db_connection()
        result = conn.execute(
            "SELECT name_device, ip_device FROM device_ip WHERE departments_id_departments = 3").fetchall()
        output = io.StringIO()
        writer = csv.writer(output)
        line = ['Gerät Name, IP Adresse']
        writer.writerow(line)
        for row in result:
            line = [str(row['name_device']) + ',' + row['ip_device']]
            writer.writerow(line)
        output.seek(0)
        return Response(output, mimetype="text/csv",
                        headers={"Content-Disposition": "attachment;filename=network_report_storage.txt"})
    except Exception as e:
        print(e)
    finally:
        conn.close()


@app.route("/donwloadTxtEinkauf")
def download_report_einkauf():
    conn = None
    try:
        conn = get_db_connection()
        result = conn.execute(
            "SELECT name_device, ip_device FROM device_ip WHERE departments_id_departments = 4").fetchall()
        output = io.StringIO()
        writer = csv.writer(output)
        line = ['Gerät Name, IP Adresse']
        writer.writerow(line)
        for row in result:
            line = [str(row['name_device']) + ',' + row['ip_device']]
            writer.writerow(line)
        output.seek(0)
        return Response(output, mimetype="text/csv",
                        headers={"Content-Disposition": "attachment;filename=network_report_buy.txt"})
    except Exception as e:
        print(e)
    finally:
        conn.close()


@app.route("/donwloadTxtVerkauf")
def download_report_verkauf():
    conn = None
    try:
        conn = get_db_connection()
        result = conn.execute(
            "SELECT name_device, ip_device FROM device_ip WHERE departments_id_departments = 5").fetchall()
        output = io.StringIO()
        writer = csv.writer(output)
        line = ['Gerät Name, IP Adresse']
        writer.writerow(line)
        for row in result:
            line = [str(row['name_device']) + ',' + row['ip_device']]
            writer.writerow(line)
        output.seek(0)
        return Response(output, mimetype="text/csv",
                        headers={"Content-Disposition": "attachment;filename=network_report_sell.txt"})
    except Exception as e:
        print(e)
    finally:
        conn.close()


if __name__ == '__main__':
    app.run()
