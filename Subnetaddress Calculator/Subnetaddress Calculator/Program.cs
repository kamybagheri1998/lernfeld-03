﻿using System.Net;

namespace Subnetaddress_Calculator
{
    internal static class Program
    {
        static readonly int[] bitValues = new int[8] { 1, 128, 64, 32, 16, 8, 4, 2 };
        static void Main(string[] args)
        {
            var myAddress = new IPAddress(new byte[4] { 10, 1, 20, 0 });
            var mySubnetmask = new Subnetmask(27);
            int myNumberOfDesiredSubnets = 2;

            var PossibleSubnets = GetPossibleSynchronousSubnets(myAddress, mySubnetmask, myNumberOfDesiredSubnets);

            foreach (var subnet in PossibleSubnets)
            {
                Console.WriteLine(subnet.Item1 + "/" + subnet.Item2.Suffix);
            }
        }

        public static IEnumerable<(IPAddress, Subnetmask)> GetPossibleSynchronousSubnets(IPAddress networkAddress, Subnetmask subnetmask, int numberOfDesiredSubnets)
        {
            if (!numberOfDesiredSubnets.IsPowerOfTwo())
                throw new InvalidOperationException("Number of desired subnets must be a power of two.");

            var networkBoundaries = GetNetworkBoundaries(networkAddress, subnetmask);
            if (!networkBoundaries.Contains(networkAddress))
            {
                throw new InvalidOperationException("Network address entered is not on a network boundary for this mask.");
            }

            int suffixDifference = (int)Math.Log2(numberOfDesiredSubnets);
            var productSubnetmask = new Subnetmask(subnetmask.Suffix + suffixDifference);

            int bytePosition = productSubnetmask.Suffix % 8;
            int octetNumber = productSubnetmask.Suffix / 8;

            for (int i = networkAddress.GetAddressBytes()[octetNumber]; i < bitValues[bytePosition] * numberOfDesiredSubnets; i += bitValues[bytePosition])
            {
                var byteCollector = new List<byte>();
                for (int j = 0; j < octetNumber; j++)
                {
                    byteCollector.Add(networkAddress.GetAddressBytes()[j]);
                }

                byteCollector.Add((byte)i);

                while (byteCollector.Count > 4)
                {
                    byteCollector.Add(0);
                }

                yield return (new IPAddress(byteCollector.ToArray()), productSubnetmask);
            }
        }

        private static IEnumerable<IPAddress> GetNetworkBoundaries(IPAddress address, Subnetmask subnetmask)
        {
            
            int bytePosition = subnetmask.Suffix % 8;
            int octetNumber = subnetmask.Suffix / 8;

            for (int i = 0; i < 256; i += bitValues[bytePosition])
            {
                var byteCollector = new List<byte>();
                for (int j = 0; j < octetNumber; j++)
                {
                    byteCollector.Add(address.GetAddressBytes()[j]);
                }

                byteCollector.Add((byte)i);

                while (byteCollector.Count > 4)
                {
                    byteCollector.Add(0);
                }

                yield return new IPAddress(byteCollector.ToArray());
            }                      
        }
    }
}