﻿
namespace Subnetaddress_Calculator
{
    internal static class Extensions
    {
        public static bool IsPowerOfTwo(this int value)
        {
            if (value == 0)
            {
                return false;
            }

            while (value != 1)
            {
                if (value % 2 != 0)
                {
                    return false;
                }

                value /= 2;
            }
            return true;
        }
    }
}
