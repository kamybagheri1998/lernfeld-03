﻿using System.Net;

namespace Subnetaddress_Calculator
{
    public class Subnetmask
    {
        public IPAddress Address { get; }
        public int Suffix { get; }

        private static readonly byte[] possibleSubnetaddressBytes = new byte[9] { 0, 128, 192, 224, 240, 248, 252, 254, 255 };

        public Subnetmask(int suffix)
        {
            Address = SuffixToIP(suffix);
            Suffix = suffix;
        }

        

        // IPToSuffix does not work yet.

        //public Subnetmask(IPAddress address)
        //{
        //    Address = address;
        //    Suffix = IPToSuffix(address);
        //}

        private static IPAddress SuffixToIP(int suffix)
        {
            if (suffix < 0 || suffix > 32)
                throw new InvalidDataException("Invalid suffix, must be between 0 and 32.");

            if (suffix == 0)
            {
                return new IPAddress(new byte[4] { 0, 0, 0, 0 });
            }

            var IP_bytes = new List<byte>();
            do
            {
                if (suffix > 8)
                {
                    suffix -= 8;
                    IP_bytes.Add(255);
                }
                else
                {          
                    IP_bytes.Add(possibleSubnetaddressBytes[suffix]);
                    suffix = 0;
                }

            } while (suffix > 0);

            while (IP_bytes.Count < 4) 
            {
                IP_bytes.Add(0);
            }

            return new IPAddress(IP_bytes.ToArray());
        }

        // Does not work yet.

        //private static int IPToSuffix(IPAddress address)
        //{
        //    var suffix = 0;

        //    var addressBytes = address.GetAddressBytes();
        //    foreach (byte octet in addressBytes)
        //    {
        //        int octetRemainder = octet;
        //        var currentBitDecimalequivalent = 128;
        //        do
        //        {
        //            if (octetRemainder >= currentBitDecimalequivalent)
        //            {
        //                suffix++;
        //                octetRemainder -= currentBitDecimalequivalent;
        //            }

        //            currentBitDecimalequivalent /= 2;

        //        } while (currentBitDecimalequivalent > 1);
        //    }

        //    return suffix;
        //}
    }
}
